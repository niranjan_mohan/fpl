package assign;

public class CopyOfCDLList<T> {
	Element firstElement;
	Element headElement;
	Writer writer;

	public CopyOfCDLList(T v){
		firstElement = new Element(v);
		firstElement.next = firstElement;
		firstElement.previous = firstElement;
		headElement = firstElement;

	}
	public CopyOfCDLList() {
		// TODO Auto-generated constructor stub
	}
	public class Element{
		T value;
		Element next;
		Element previous;
		Element(T value){
			this.value = value;
		}
		public T value(){
			return value;
		}
	}
	public Element head(){
		return headElement;
	}
	public Cursor reader(Element from){
		//find out the element and then pass the reader from that element 
		Cursor cursor = new Cursor();
		boolean flag = true;int count =0;
		while (flag){
			//search list for element
			if(cursor.current() == from ){
				System.out.println("Element found");
				return cursor;
			}
			cursor.next();
			if(cursor.current() == headElement)
				count ++;
			if(count == 2){
				System.out.println("Element not found in list returning the firstElement to reader");
				throw new RuntimeException("Invalid element");
			}
		}
		return cursor;

	}
	public class Cursor {
		Element curElement;
		
		Cursor(Element from){
			this.curElement = from;
		}
		Cursor(){
			this.curElement = firstElement;
		}
		public Element current(){
			return curElement;
		}
		public void previous(){
			curElement = curElement.previous;
		}
		public void next(){
			curElement = curElement.next;
		}
		public Writer writer(){
			if(writer == null)
				writer = new Writer(curElement);
			return writer;
		}
	}
	public class Writer{
		Element curWriter;
		Writer(Element curElement){
			this.curWriter = curElement;
		}
		Writer(){
			if(curWriter !=null )
				this.curWriter = headElement;
		}

		public boolean insertBefore(T val){
			//check if list is empty
			Element newElement = new Element(val);
			if(curWriter != null){

				if(curWriter.next == curWriter){
					//code to add when there is only one element in list
					System.out.println("adding item 2 into the list");
					newElement.previous = curWriter;
					newElement.next = curWriter;
					curWriter.next = newElement;
					curWriter.previous = newElement;
					curWriter = newElement;
					firstElement = curWriter;
				}
				else{
					//code for adding when 2 elements have been added to the list
					System.out.println("cur writer val is "+curWriter.value);
					System.out.println("adding 2 elements adding the others");
					newElement.next = curWriter;
					newElement.previous = curWriter.previous;
					curWriter.previous.next = newElement;
					curWriter.previous = newElement;
					curWriter = newElement; 
					firstElement = curWriter;
				}
			}
			else{
				System.out.println(" ************Error the current element passed has no value ************** ");
				return false;
			}
			return true;
		}
		public boolean insertAfter(T val){
			//check if list is empty
			Element newElement = new Element(val);
			if(curWriter != null){

				if(curWriter.next == curWriter){
					//code to add when there is only one element in list
					newElement.previous = curWriter;
					newElement.next = curWriter;
					newElement.next = curWriter;
					newElement.previous = curWriter;
					newElement = curWriter;
					firstElement = curWriter;
				}
				else{
					//code for adding when 2 elements have been added to the list
					//printing current writer for testing
					System.out.println("cur writer val is "+curWriter.value);
					newElement.previous = curWriter;
					newElement.next = curWriter.next;
					curWriter.next.previous = newElement;
					curWriter.next = newElement;
					curWriter = newElement; 
					firstElement = curWriter;
				}
			}
			else{
				System.out.println(" ************Error the current element passed has no value ************** ");
				return false;
			}
			return true;
		}
		public boolean delete(){
			//check if the element is the head element
			System.out.println("value of writer"+curWriter.value);
			if(curWriter != headElement){
				curWriter.previous.next = curWriter.next;
				curWriter.next.previous = curWriter.previous;
				curWriter.next =null;
				curWriter.previous=null;
				curWriter = null;
				
			}
			else {
				System.out.println("cannot delete head Element");
				return false;
			}
			return true;
		}
	}

	//Code written for testing should be deleted
	public void displayAll(){
		System.out.println("moving forward");
		Element curElement = headElement;
		firstElement= headElement;
		outerloop:
			while (firstElement != null){
				System.out.println("element ->"+firstElement.value);
				firstElement = firstElement.next;
				if (firstElement == curElement){
					System.out.println("need to exit ");
					break outerloop;
				}
			}

	}


}
