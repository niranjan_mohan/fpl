package assign.subtest;




public class CDLListFine<T> extends CDLList {
	boolean cursorLock ;
	Cursor cursor;
	Writer writer;
	CDLList<T>.Element headElement ;
	public CDLListFine(T v) {
		super(v);
		headElement= super.headElement;
		System.out.println("reference of head in finee list-->"+headElement);
		System.out.println("reference of head in finee list-->"+headElement);
		//insert phantom element here
		T val = null;
		super.reader(headElement).writer().insertBefore(val);
	}



	public  Cursor reader (CDLList.Element from){
		cursor = new Cursor(from);
		return cursor;
	}


	public class Element extends CDLList<T>.Element{
		T value;
		Element(T value) {
			super(value);
			// TODO Auto-generated constructor stub
		}
	}

	public class Cursor extends CDLList<T>.Cursor{

		Cursor(CDLList.Element from){
			super(from);
		}
		public   void next(){
			synchronized(super.curElement){
				if(super.curElement.next.next ==  headElement){
				super.next();super.next();
				}
				else{
					super.next();
				}
			}


		}
		public  void previous(){
			synchronized(super.curElement){
				if(super.curElement == headElement)
				super.previous();
				super.previous();
			}
		}
		public Writer writer(){
			//f(writer == null)
			writer = new Writer(super.curElement);
			return writer;
		}
	}


	public class Writer extends CDLList<T>.Writer{
		boolean flag;

		Writer(CDLList<T>.Element curElement){
			this.curWriter = curElement;
		}
		public  boolean insertBefore(T val){
			//need to check if the lock is already taken
			synchronized(super.curWriter.previous){
				synchronized(super.curWriter){
					return super.insertBefore(val);
				}
			}
		}
		public  boolean insertAfter(T val){
			//need to check if the lock is already taken
			synchronized (super.curWriter){
				synchronized(super.curWriter.next){
					//System.out.println("got lock writing list");
					return super.insertAfter(val);
				}
			}
		}
		public  boolean delete(){
			//try to get lock of cursor before operation
			//need to check if the lock is already taken
			synchronized(super.curWriter){
				synchronized(super.curWriter.next){
					synchronized(super.curWriter.previous){
						return super.delete();
					}
				}
			}
		}
	}
}

