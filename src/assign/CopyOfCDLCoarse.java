package assign;
/**
 * 
 */


import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;



/**
 * @author Niranjan Mohan
 * nmohan4@buffalo.edu
 */
public class CopyOfCDLCoarse<T> extends CDLList<T>  {
	Lock lock = new ReentrantLock();
	boolean cursorLock ;
	Cursor cursor;
	Writer writer;
	public CopyOfCDLCoarse(T v) {
		super(v);
	}



	public  Cursor reader (CDLList.Element from){
		cursor = new Cursor(from);
		return cursor;
	}


	public class Element extends CDLList<T>.Element{
		T value;
		Element(T value) {
			super(value);
			// TODO Auto-generated constructor stub
		}
		public class Cursor extends CDLList<T>.Cursor{
			Cursor(CDLList.Element from ){
				super(from);
			}
		}
	}

	public class Cursor extends CDLList<T>.Cursor{
		
		Cursor(CDLList.Element from){
			super(from);
		}
		
		public Writer writer(){
			//f(writer == null)
				writer = new Writer(super.curElement);
			return writer;
		}
	}


	public class Writer extends CDLList<T>.Writer{
		boolean flag;

		Writer(CDLList<T>.Element curElement){
			this.curWriter = curElement;
		}
		public  boolean insertBefore(T val){
			//try to get lock of cursor before operation
			cursorLock = CopyOfCDLCoarse.this.lock.tryLock();//.CDLCoarse.this.trylock();
			System.out.println("pass");
			while (!cursorLock){
				System.out.println("unable to get lock");
				//put current thread to sleep
				Thread.yield();
			}
			flag = super.insertBefore(val);
			lock.unlock();
			Thread.currentThread().notifyAll();
			return flag;
		}
		public  boolean insertAfter(T val){
			//try to get lock of cursor before operation
			cursorLock = CopyOfCDLCoarse.this.lock.tryLock();//.CDLCoarse.this.trylock();

			while (!cursorLock){
				System.out.println("unable to get lock");
				//put current thread to sleep
				
				try{
					System.out.println("going to sleep");
					Thread.currentThread().wait();
					//Thread.sleep(1000);
				}
				catch(Exception e){
					e.printStackTrace();
					System.out.println(e.getMessage());
				}
			}
			flag=  super.insertAfter(val);
			lock.unlock();
			Thread.currentThread().notifyAll();
			return flag;
		}
		public  boolean delete(){
			//try to get lock of cursor before operation
			cursorLock = CopyOfCDLCoarse.this.lock.tryLock();//.CDLCoarse.this.trylock();
			while (!cursorLock){
				System.out.println("unable to get lock");
				//put current thread to sleep

				Thread.yield();
			}
			flag= super.delete();
			lock.unlock();
			Thread.currentThread().notifyAll();
			return flag;
		}
	}

}
