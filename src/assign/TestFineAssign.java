package assign;

public class TestFineAssign implements Runnable {
	CDLListFine<String> cdf;
	public TestFineAssign() {

	}

	public TestFineAssign(String ThreadName) {
		cdf = new CDLListFine<String>(ThreadName);
	}
	public void run(){
		System.out.println("enter run ");
		//going to add values 
		for (int i=0;i<5;i++){
			String s= Thread.currentThread().getName()+" - :"+i;
			addValueBefore(s);
			Display();
		}
		for (int i=1000 ;i>998;i--){
			String s= Thread.currentThread().getName()+" - :"+i;
			addValueAfter(s);
		}
	
		Display();

	}
	public void Display(){
		cdf.displayAll();
	}
//	public void DisplayList(){
//		 System.out.println("-->"+cdf.cursor.curElement.value);
//		 cdf.cursor.next();
//		}

	public void addValueBefore(String Val){
		CDLListFine<String>.Cursor c = cdf.reader(cdf.headElement);
		c.writer().insertBefore(Val);

	}
	public void addValueAfter(String Val){

		CDLListFine<String>.Cursor c = cdf.reader(cdf.headElement);
		//System.out.println("going to call add value after");
		c.writer().insertAfter(Val);
		//System.out.println("after calling ");
	}
	public static void main(String [] args){
		TestFineAssign c1 = new TestFineAssign("Instance 1");
		for (int i=0;i<20;i++){
		Thread t1 = new Thread(c1);
		Thread t2 = new Thread(c1);
		Thread t3 = new Thread(c1);
		Thread t4 = new Thread(c1);
		Thread t5 = new Thread(c1);
		
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		c1.Display();
		}
//		
//		if(! t1.isAlive() && ! t2.isAlive() && ! t3.isAlive()){
//			System.out.println("********Displaying the list******************");
//			c1.Display();
//		}
//		

		
		
	}


}
