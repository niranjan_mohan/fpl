package assign;


import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class Driver {
    @Test
    public void test1(){
        
        CDLList<String> list = new CDLList<String>("hi");
        CDLList.Element head = list.head();
        CDLList.Cursor c = list.reader(list.head());
        
        for(int i = 74; i >= 65; i++) {
            char val = (char) i;
            c.writer().insertAfter("" + val);
        }
        
        List<Thread> threadList = new ArrayList<Thread>();
        for (int i = 0; i < 5; i++) {
            NormalThread nt = new NormalThread(list, i);
            threadList.add(nt);
        }
            RandomThread rt = new RandomThread(list);
            threadList.add(rt);
        try {
            for(Thread t : threadList)
                t.join();
        } catch(InterruptedException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
    }
}


