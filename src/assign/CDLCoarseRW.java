package assign;

/**
 * @author Niranjan Mohan
 * nmohan4@buffalo.edu
 */

public class CDLCoarseRW<T> extends CDLList<T> {
	Cursor cursor;
	Writer writer;
	ReadWriteLock rwLock = new ReadWriteLock();
	public CDLCoarseRW(T v) {
		super(v);
	}

	public  Cursor reader (CDLList.Element from){
		cursor = new Cursor(from);
		return cursor;
	}

	public class Cursor extends CDLList<T>.Cursor{

		Cursor(CDLList.Element from){
			super(from);
		}
		public   void next(){
			if(rwLock.lockRead())
			super.next();
			rwLock.unlockRead();
		}
		public  void previous(){
			if(rwLock.lockRead())
			super.previous();
			rwLock.unlockRead();
		}
		public Writer writer(){
			//f(writer == null)
			writer = new Writer(super.curElement);
			return writer;
		}
	}


	public class Writer extends CDLList<T>.Writer{
		boolean flag;

		Writer(CDLList<T>.Element curElement){
			this.curWriter = curElement;
		}
		public  boolean insertBefore(T val){
			if (rwLock.lockWrite() ){
				super.insertBefore(val);
				rwLock.unlockWrite();
				return true;
			}
			else{
				System.out.println("thread waiting ");
			}
			return false;
		}
		public  boolean insertAfter(T val){
			//check id the lock needs to be released wait if so
			if (rwLock.lockWrite() ){
				super.insertAfter(val);
				rwLock.unlockWrite();
				return true;
			}
			else{
				System.out.println("thread waiting ");
			}
			return false;
		}

		public  boolean delete(){
			if (rwLock.lockWrite() ){
				super.delete();
				rwLock.unlockWrite();
				return true;
			}
			else{
				System.out.println("thread waiting ");
			}
			return false;
		}
	}

}




