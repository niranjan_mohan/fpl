package assign;

import assign.CDLList.Element;


public class CDLListFine<T> extends CDLList {
	boolean cursorLock ;
	Cursor cursor;
	Writer writer;
	CDLList<T>.Element headElement ;
	public CDLListFine(T v) {
		super(v);
		headElement= super.headElement;
		//		System.out.println("reference of head in finee list-->"+headElement);
		//		System.out.println("reference of head in finee list-->"+headElement);
		//insert phantom element here
		T val = null;
		super.reader(headElement).writer().insertBefore(val);
	}
	public  Cursor reader (CDLList.Element from){
		cursor = new Cursor(from);
		return cursor;
	}


	//	public class Element extends CDLList<T>.Element{
	//		Element(T value) {
	//			super(value);
	//			// TODO Auto-generated constructor stub
	//		}
	//	}

	public class Cursor extends CDLList<T>.Cursor{

		Cursor(CDLList.Element from){
			super(from);
		}
		public   void next(){
			synchronized(super.curElement){

				if(super.curElement.next.next ==  headElement){
					//if(super.curElement.next.value == null){
					super.next();
					super.next();
				}
				else{
					super.next();
				}
			}
		}
		public  void previous(){
			synchronized(super.curElement){
				//System.out.println("current element in prev function :"+curElement.value);
				if(super.curElement == headElement){
					super.previous();
					super.previous();
				}
				else{
					super.previous();
				}
			}
		}

		public Writer writer(){
			//f(writer == null)
			writer = new Writer(super.curElement);
			return writer;
		}
	}


	public class Writer extends CDLList<T>.Writer{
		boolean swapFlag ;

		Writer(CDLList<T>.Element curElement){
			this.curWriter = curElement;
		}
		public  boolean insertBefore(T val){
			//need to check if the lock is already taken
			//if the cur element is head need to handle
			System.out.println("****insert before****");
			boolean retFlag = true;
			boolean tempFlag = true;
			while(tempFlag){
				Element tempEl = super.curWriter.previous;
				synchronized(tempEl){
					synchronized(super.curWriter){
						if(tempEl == super.curWriter.previous){
							tempFlag = false;
							retFlag= super.insertBefore(val);
							if (super.curWriter == headElement)
								swapFlag=true;//swap elements 
							if(swapFlag){
								//swap the values let the elements remain
								T temp = super.curWriter.previous.value;
								super.curWriter.previous.value =super.curWriter.previous.previous.value;
								super.curWriter.previous.previous.value = temp;
								retFlag = true;
								swapFlag = false;
							}
						}
					}
				}
			}
			return retFlag;
		}
		public  boolean insertAfter(T val){
			//need to check if the lock is already taken
			//if the cur element is head need to handle 
			//unreachable code as curwriter can never be phanthom element
			System.out.println("INSERT AFTER val:"+val);
			//if (super.curWriter.next == headElement)
			synchronized (super.curWriter){
				synchronized(super.curWriter.next){
					//System.out.println("Cur writer value in insert after"+curWriter.value);
					return super.insertAfter(val);
				}
			}
		}
//		public  boolean delete(){
//			//try to get lock of cursor before operation
//			//need to check if the lock is already taken
//			synchronized(super.curWriter){
//				synchronized(super.curWriter.next){
//					synchronized(super.curWriter.previous){
//						return super.delete();
//					}
//				}
//			}
//		}
	}
}

