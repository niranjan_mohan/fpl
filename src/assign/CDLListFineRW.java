package assign;





public class CDLListFineRW<T> extends CDLList {
	boolean cursorLock ;
	Cursor cursor;
	Writer writer;
	CDLList<T>.Element headElement ;
	public CDLListFineRW(T v) {
		super(v);
		headElement= super.headElement;
		//		System.out.println("reference of head in finee list-->"+headElement);
		//		System.out.println("reference of head in finee list-->"+headElement);
		//insert phantom element here
		T val = null;
		super.reader(headElement).writer().insertBefore(val);
	}

	public  Cursor reader (CDLList.Element from){
		cursor = new Cursor(from);
		return cursor;
	}
	public class Cursor extends CDLList<T>.Cursor{

		Cursor(CDLList.Element from){
			super(from);
		}
		public   void next(){
			boolean phantomFlag = false;
			//try to get read lock of the element
			if(super.curElement.rwLock.lockRead()){
				if(super.curElement.next.next ==  headElement){
					super.next();
					super.next();
					phantomFlag = true;
				}
				else{
					super.next();
				}
			}
			if(phantomFlag)
				super.curElement.previous.previous.rwLock.unlockRead();
			else
				super.curElement.previous.rwLock.unlockRead();
		}
		public  void previous(){
			boolean phantomFlag = false;
			//try to get read lock of the Element
			if(super.curElement.rwLock.lockRead()){
				//System.out.println("current element in prev function :"+curElement.value);
				if(super.curElement == headElement){
					phantomFlag = true;
					super.previous();
					super.previous();
				}
				else{
					super.previous();
				}
			}
			if(phantomFlag)
				super.curElement.next.next.rwLock.unlockRead();
			else
				super.curElement.next.rwLock.unlockRead();
		}

		public Writer writer(){
			//f(writer == null)
			writer = new Writer(super.curElement);
			return writer;
		}
	}


	public class Writer extends CDLList<T>.Writer{
		boolean swapFlag ;

		Writer(CDLList<T>.Element curElement){
			this.curWriter = curElement;
		}
		public  boolean insertBefore(T val){
			boolean tempFlag=true;
			//need to check if the lock is already taken
			//if the cur element is head need to handle
			System.out.println("****insert before****");
			boolean retFlag = true;
			//try to lockWrite the Element(s) 
			while(tempFlag){
				Element tempEl = super.curWriter.previous;
				if(tempEl.rwLock.lockWrite()){
					if(tempEl != super.curWriter){
						if(super.curWriter.rwLock.lockWrite()){
							System.out.println("locked -->"+super.curWriter);
							System.out.println("locked -->"+tempEl);
							//temp check
							if(tempEl == super.curWriter.previous){
								System.out.println("passed");
								tempFlag = false;
								retFlag= super.insertBefore(val);
								if (super.curWriter == headElement)
									swapFlag=true;//swap elements 
								if(swapFlag){
									//swap the values let the elements remain
									T temp = super.curWriter.previous.value;
									super.curWriter.previous.value =super.curWriter.previous.previous.value;
									super.curWriter.previous.previous.value = temp;
									swapFlag = false;
								}
							}
							else//temp was not equal to previous going to try to get locks again
								System.out.println("failed");
						}


						if(tempFlag){
							//was unable to perform insert need to unlock old objects
							System.out.println("unlocking objects-->"+super.curWriter);
							System.out.println("unlocking objects-->"+tempEl);
							super.curWriter.rwLock.unlockWrite();
							tempEl.rwLock.unlockWrite();
						}
						else{
							//performed insert need to unlock
							System.out.println("unlocking objects-->"+super.curWriter);
							System.out.println("unlocking objects-->"+super.curWriter.previous.previous);
							super.curWriter.rwLock.unlockWrite();
							super.curWriter.previous.previous.rwLock.unlockWrite();
						}
					}
					else{
						//the tempE1 became the curwriter before getting the lock.try again.
						tempEl.rwLock.unlockWrite();
					}
				}//end if temp rw lock
			}//end while loop
			return retFlag;
		}
		public  boolean insertAfter(T val){
			System.out.println("****insert AFTER****");
			boolean retFlag =true;
			//try to lock write the elements
			if (super.curWriter.rwLock.lockWrite()){
				if(super.curWriter.next.rwLock.lockWrite()){
					retFlag= super.insertAfter(val);
				}
			}
			super.curWriter.next.next.rwLock.unlockWrite();
			super.curWriter.rwLock.unlockWrite();
			return retFlag;
		}
//		public  boolean delete(){
//			//try to get lock of cursor before operation
//			//need to check if the lock is already taken
//			synchronized(super.curWriter){
//				synchronized(super.curWriter.next){
//					synchronized(super.curWriter.previous){
//						return super.delete();
//					}
//				}
//			}
//		}
	}
}
