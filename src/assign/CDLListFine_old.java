package assign;



public class CDLListFine_old<T> extends CDLList {
	boolean cursorLock ;
	Cursor cursor;
	Writer writer;
	public CDLListFine_old(T v) {
		super(v);
	}



	public  Cursor reader (CDLList.Element from){
		cursor = new Cursor(from);
		return cursor;
	}


	public class Element extends CDLList<T>.Element{
		T value;
		Element(T value) {
			super(value);
			// TODO Auto-generated constructor stub
		}
		public class Cursor extends CDLList<T>.Cursor{
			Cursor(CDLList.Element from ){
				super(from);
			}
		}
	}

	public class Cursor extends CDLList<T>.Cursor{

		Cursor(CDLList.Element from){
			super(from);
		}
		public   void next(){
			synchronized(super.curElement){
				synchronized(super.curElement.next)
				{
					super.next();
				}
			}


		}
		public  void previous(){
			synchronized(super.curElement){
				synchronized(super.curElement.previous){
					super.previous();
				}
			}
		}
		public Writer writer(){
			//f(writer == null)
			writer = new Writer(super.curElement);
			return writer;
		}
	}


	public class Writer extends CDLList<T>.Writer{
		boolean flag;

		Writer(CDLList<T>.Element curElement){
			this.curWriter = curElement;
		}
		public  boolean insertBefore(T val){
			synchronized(super.curWriter){
				synchronized(super.curWriter.previous){
					return super.insertBefore(val);
				}
			}
		}
		public  boolean insertAfter(T val){
			synchronized (super.curWriter){
				synchronized(super.curWriter.next){
					System.out.println("got lock writing list");
					return super.insertAfter(val);
				}
			}
		}
		public  boolean delete(){
			//try to get lock of cursor before operation
			synchronized(super.curWriter){
				synchronized(super.curWriter.next){
					synchronized(super.curWriter.previous){
						return super.delete();
					}
				}
			}
		}
	}
}

