package assign;
/**
 * 
 */






/**
 * @author Niranjan Mohan
 * nmohan4@buffalo.edu
 */
public class CDLCoarse<T> extends CDLList<T>  {
	Cursor cursor;
	Writer writer;
	public CDLCoarse(T v) {
		super(v);
	}



	public  Cursor reader (CDLList.Element from){
		cursor = new Cursor(from);
		return cursor;
	}

	public class Cursor extends CDLList<T>.Cursor{
		
		Cursor(CDLList.Element from){
			super(from);
		}
		public  synchronized void next(){
			super.next();
		}
		public synchronized void previous(){
			super.previous();
		}
		public Writer writer(){
			//f(writer == null)
				writer = new Writer(super.curElement);
			return writer;
		}
	}


	public class Writer extends CDLList<T>.Writer{
		boolean flag;

		Writer(CDLList<T>.Element curElement){
			this.curWriter = curElement;
		}
		public synchronized boolean insertBefore(T val){
			return super.insertBefore(val);
		}
		public synchronized boolean insertAfter(T val){
			return super.insertAfter(val);
		}
		public synchronized boolean delete(){
			//try to get lock of cursor before operation
			return super.delete();
		}
	}

}
