

import java.util.concurrent.atomic.AtomicInteger;

public class CopyOfCDLListRW {
	private AtomicInteger lock =new AtomicInteger();
	Object mylock;
	//int writerQ;
	int temp;

	CopyOfCDLListRW(){
		lock.set(1);
		mylock = new Object();
		//writerQ=0;
	}
	public CopyOfCDLListRW lockRead()throws InterruptedException{
		//if not writers are in Q
		System.out.println("in lock read"+lock.get());
		while(true){
			if( (temp = lock.get()) > 1){
				//when no writer is in Q and the lock is with reader
				synchronized(mylock){
					if(temp == lock.get()){
						System.out.println("when no writer is in Q or the lock unlocked-->"+lock.get());
						while(!lock.compareAndSet(temp, temp+1));
						return this;
					}
				}
			}
			else if((temp =lock.get()) <= 0) {//either writer holds the lock or writer is on wait should not contend
				synchronized(mylock){
					if(temp==lock.get()){
						System.out.println("read lock waiting"+lock.get());
						mylock.wait();
					}
				}
			}
			else{//if value is 1 contend for lock

				if((temp = lock.get())==1){
					synchronized(mylock){
						if(temp == lock.get()){
							while(!lock.compareAndSet(temp, temp+1));
							System.out.println("got read lock"+lock.get());
							return this;
						}
						else{
							System.out.println("YYYYYYYYYYYYYYYYYY");
							mylock.wait();
						}
					}
				}
			}
		}//while true
	}
	public void unlockRead(){
		System.out.println("in unlock read"+lock.get());
		while(true){
			System.out.println("in unlock read");
			if((temp=lock.get()) < 0){//lock is with reader buffer
				synchronized(mylock){
					if(temp==lock.get()){
						//System.out.println("decreamenting reader"+temp);
//						if(lock.get() ==-1)
//							while(!lock.compareAndSet(temp, temp+2));
//						else
							while(!lock.compareAndSet(temp, temp+1));
						mylock.notifyAll();
						System.out.println("notified all after decreament"+lock.get());
						return;
					}
				}
			}
			//no writers in queue
			else if((temp=lock.get())> 1){
				synchronized(mylock){
					if(temp==lock.get()){
						//lock.incrementAndGet();
						while(!lock.compareAndSet(temp,temp-1));
						System.out.println("going to notify all in unlock read"+lock.get());
						mylock.notifyAll();
						return;
					}
				}
			}
			else{
				//should not be here
				//System.out.println("in else in unlock writer");
				mylock.notifyAll();
			}
		}
	}
	public CopyOfCDLListRW lockWrite()throws InterruptedException{
		System.out.println("in lock write"+lock.get());
		while(true){
			System.out.println("in lock write");
			if((temp=lock.get()) >1){
				synchronized(mylock){
					if(temp == lock.get()){
						while(!lock.compareAndSet(temp, temp*-1));
						System.out.println("write lock waiting"+lock.get());
						mylock.wait();
					}

				}
			}
			else if ((temp=lock.get()) <= 0){
				System.out.println("in write lock else if "+lock.get());
				synchronized(mylock){
					if(temp == lock.get()){
						if(temp == -1){
							while(!lock.compareAndSet(-1, 0));
							return this;
						}
						System.out.println("lock write wainting");
						mylock.wait();
					}
				}
			}
			else {//try to get the lock writer
				System.out.println("in write lock else"+lock.get());
				temp = lock.get();
				synchronized(mylock){
					if(temp == 1){
						while(!lock.compareAndSet(1, 0));
						System.out.println("got write lock"+lock.get());	
					}
					else{
						System.out.println("write lock waiting"+lock.get());
						mylock.wait();
					}
					System.out.println("got lock going to return"+lock.get());
					return this;
				}
			}
		}
	}
	public void unlockWrite(){
		System.out.println("in unlock write"+lock.get());
		synchronized(mylock){
			while(!lock.compareAndSet(0, 1));
			System.out.println("unlocked write going to notify all"+lock.get());
			mylock.notifyAll();
		}
	}
}
