package luke;

/**
* CDLList is a doubly linked circular list.
*
* @author Lukasz Ziarek
* @author Sree Harsha Konduri
* @author Namita Vishnubhotla
*
*/

public class CDLCoarse<T> extends CDLList<T>{

    protected Element tail; //internal use only
    private Object lock;

    CDLCoarse(T v){
		super(v);
		tail = new Element(v);
		lock = new Object();
		head.value = v;
		tail.value = v;
		tail.deleted = false;
		head.deleted = false;
		head.previous = head.next = tail;
		tail.previous = tail.next = head;
    }

   public CDLList<T>.Element head() {
       return head;
   }
  
   public Cursor reader(CDLList<T>.Element from) {
       return new Cursor(from);
   }

   public class Cursor extends CDLList<T>.Cursor{

       public Cursor(CDLList<T>.Element c) {
		   super(c);
       }
       public CDLList<T>.Element current() {
           synchronized(lock){
               if(current.deleted)
                   return head;
               else
                   return current;
           }
       }
       public void previous() {
           if(current == head){ 
			   synchronized(lock){
					current = tail.previous;
			   }
			}
			else{
				synchronized(lock){
					if(current.deleted)
						current=head;
					current = current.previous;
				}
                   
			}
       }

       public void next() {
           synchronized(lock){
			   if(current.deleted){
				   current = head;
			   }
			   if(current.next == tail) {			   
				   current = head;
			   }
			   else {
				   current = current.next;
			   }
		   }
       }

       public Writer writer() {
           return new Writer(current);
       }
   }

   public class Writer extends CDLList<T>.Writer{
       volatile CDLList<T>.Element temp1;

       public Writer(CDLList<T>.Element c) {
		   super(c);
       }

       public boolean delete()
       {
    	   try {
    	   CDLList<T>.Element ele=delete(this);
    	   if(ele!=null) {
    		   return true;
    	   }
    	   }
    	   catch(Exception e) {
    		   e.printStackTrace();
    	   }
    	   return false;
       }
        CDLList<T>.Element delete(Writer w) throws deleteException{
           if(cursor.current.deleted || cursor.current == head || cursor.current == tail){
               throw new deleteException();
           }
           while (true){
               temp1 = cursor.current.previous;
               synchronized(lock){
                       if(temp1 == cursor.current.previous){
                           
                               if(cursor.current.deleted)
                                   throw new deleteException();
                               cursor.current.previous.next = cursor.current.next;
			       cursor.current.next.previous = cursor.current.previous;                         
                               cursor.current.deleted = true;
                               return cursor.current;                                                  
                   }
               }
           }
       }

       public boolean insertBefore(T val) {
	       CDLList.Element temp = new CDLList.Element(val);
	       temp.deleted = false;
	       temp.value = val;
           if(cursor.current == head){
	           while(true){
				   temp1 = cursor.current.previous;
					   synchronized(lock){
						   if(temp1 == cursor.current.previous){
							   temp.previous = (CDLList.Element) tail.previous;
							   temp.next = (CDLList.Element) tail;
							   tail.previous.next = temp;
							   tail.previous = temp;
							   return true;						   
					   }
				   }
			   }
           }else{
               while(true){
                   temp1 = cursor.current.previous;
                       synchronized(lock){
                           if(cursor.current.deleted)
                               return false;
                           if(temp1 == cursor.current.previous){
                               temp.previous = (CDLList.Element) cursor.current.previous;//check
                               temp.next = (CDLList.Element) cursor.current;//check
                               cursor.current.previous.next = temp;
                               cursor.current.previous = temp;
							   return true;                           
                       }
                   }
               }
           }
       }

       public boolean insertAfter(T val) {
		   CDLList.Element temp = new CDLList.Element(val);
		   temp.deleted = false;
		   temp.value = val;
			   synchronized(lock){
				   if(cursor.current.deleted)
					   return false;
				   temp.previous = (CDLList.Element) cursor.current;//check
				   temp.next = (CDLList.Element) cursor.current.next;//check
				   cursor.current.next.previous = temp;
				   cursor.current.next = temp;			   
		   }
	   
           return true;
       }
   }
} 