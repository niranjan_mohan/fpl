package luke;

/**
* CDLList is a doubly linked circular list.
*
* @author Lukasz Ziarek
* @author Sree Harsha Konduri
* @author Namita Vishnubhotla
*
*/

public class CDLListRW {
        private int readers       = 0;
        private int writers       = 0;
        private int writes = 0;
       
        synchronized CDLListRW lockRead() throws InterruptedException{
        while(writers > 0 || writes > 0){
        	System.out.println("Wait lock read");
        	wait();
            }
            readers++;
	    return this;
        }
       
        synchronized void unlockRead(){
            readers--;
            notifyAll();
        }
       
        synchronized CDLListRW lockWrite() throws InterruptedException{
            writes++;
            while(readers > 0 || writers > 0){
            	System.out.println("wait in lock write");
                wait();
            }
            writes--;
            writers++;
	    return this;
        }
   
        synchronized void unlockWrite() throws InterruptedException{
            writers--;
            notifyAll();
        }   
 }