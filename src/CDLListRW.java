

import java.util.concurrent.atomic.AtomicInteger;

public class CDLListRW {
	private AtomicInteger lock =new AtomicInteger();
	Object mylock;
	//int writerQ;
	int temp;

	CDLListRW(){
		lock.set(1);
		mylock = new Object();
		//writerQ=0;
	}
	public CDLListRW lockRead()throws InterruptedException{
		//if not writers are in Q
		while(true){
			if( (temp = lock.get()) > 1){
				//when no writer is in Q and the lock is with reader
				synchronized(mylock){
					if(temp == lock.get()){
						while(!lock.compareAndSet(temp, temp+1));
						return this;
					}
				}
			}
			else if((temp =lock.get()) <= 0) {//either writer holds the lock or writer is on wait should not contend
				synchronized(mylock){
					if(temp==lock.get()){
						mylock.wait();
					}
				}
			}
			else{//if value is 1 contend for lock

				if((temp = lock.get())==1){
					synchronized(mylock){
						if(temp == lock.get()){
							while(!lock.compareAndSet(temp, temp+1));
							return this;
						}
						else{
							mylock.wait();
						}
					}
				}
			}
		}//while true
	}
	public void unlockRead(){
		while(true){
			if((temp=lock.get()) < 0){//lock is with reader buffer
				synchronized(mylock){
					if(temp==lock.get()){
						//System.out.println("decreamenting reader"+temp);
//						if(lock.get() ==-1)
//							while(!lock.compareAndSet(temp, temp+2));
//						else
							while(!lock.compareAndSet(temp, temp+1));
						mylock.notifyAll();
						return;
					}
				}
			}
			//no writers in queue
			else if((temp=lock.get())> 1){
				synchronized(mylock){
					if(temp==lock.get()){
						//lock.incrementAndGet();
						while(!lock.compareAndSet(temp,temp-1));
						mylock.notifyAll();
						return;
					}
				}
			}
			else{
				//should not be here
				//System.out.println("in else in unlock writer");
				mylock.notifyAll();
			}
		}
	}
	public CDLListRW lockWrite()throws InterruptedException{
		while(true){
			if((temp=lock.get()) >1){
				synchronized(mylock){
					if(temp == lock.get()){
						while(!lock.compareAndSet(temp, temp*-1));
						mylock.wait();
					}

				}
			}
			else if ((temp=lock.get()) <= 0){
				synchronized(mylock){
					if(temp == lock.get()){
						if(temp == -1){
							while(!lock.compareAndSet(-1, 0));
							return this;
						}
						mylock.wait();
					}
				}
			}
			else {//try to get the lock writer
				temp = lock.get();
				synchronized(mylock){
					if(temp == 1){
						while(!lock.compareAndSet(1, 0));
						return this;
					}
					else{
						mylock.wait();
					}
				}
			}
		}
	}
	public void unlockWrite(){
		
		synchronized(mylock){
			while(!lock.compareAndSet(0, 1));
			mylock.notifyAll();
		}
	}
}
