package old;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;







public class CDLListFine_test<T> extends CDLList {
	Element firstElement;
	Element headElement;
	Cursor cursor;
	Writer writer;
	public CDLListFine_test(T v) {
		firstElement = new Element(v);
		firstElement.next = firstElement;
		firstElement.previous = firstElement;
		headElement = firstElement;
	}



	public  Cursor reader (Element from){
		cursor = new Cursor(from);
		return cursor;
	}


	public class Element extends CDLList<T>.Element{
		T value;
		Element next;
		Element previous;
		final Lock mylock = new ReentrantLock();
		final Condition ltaken = mylock.newCondition();

		public T value(){
			return value;
		}

		Element(T value) {
			super(value);
			this.value = value;
			// TODO Auto-generated constructor stub
		}
		public class Cursor extends CDLList<T>.Cursor{
			Cursor(CDLList.Element from ){
				super(from);
			}
		}
	}

	public class Cursor extends CDLList<T>.Cursor{
		Element curElement;
		Cursor(Element from){
			this.curElement = from;
		}
		//should not be able to read if the element and its next element is locked
		public   void next(){
			curElement = curElement.next;
		}
		//should not be able to read if the element and the previous element is locked
		public  void previous(){
			curElement = curElement.previous;
		}
		public Writer writer(){
			//f(writer == null)
			writer = new Writer(curElement);
			return writer;
		}
	}


	public class Writer extends CDLList<T>.Writer{
		boolean flag;
		Element curWriter;
		Element tempNode;
		boolean flock1 =false;
		boolean flock2 =false;

		Writer(Element curElement){
			this.curWriter = curElement;
		}

		public  boolean insertBefore(T val){
			return super.insertBefore(val);
		}
		public  boolean insertAfter(T val){

			//check if list is empty
			Element newElement = new Element(val);
			if(curWriter != null){
				//try to obtain the lock of current writer and next of current writer
				try{
					flag = true;
					if(flock1 =curWriter.mylock.tryLock())
						flock2 = curWriter.next.mylock.tryLock();

					while (flag){

						if(flock1 && flock2){
							//proceed with the code
							System.out.println("locks taken by thread"+ Thread.currentThread().getName());
							System.out.println("object curwriter whose lock is taken-->"+curWriter+"-->"+curWriter.next+"** "+Thread.currentThread().getName());
							flag = false;
						}

						else {
							System.out.println("unable to take lock-->"+ flock1 +"-->"+ Thread.currentThread().getName());
							curWriter.next.ltaken.await(100l,TimeUnit.MILLISECONDS);
							if(flock1){
								curWriter.mylock.unlock();
								curWriter.ltaken.signalAll();
								flock1 = false;
							}

						}
					}
				}


				catch (InterruptedException e){
					e.printStackTrace();
				}
				finally{
					
					System.out.println("the curwriter before the operation is");
					tempNode = curWriter.next;//after "insert after" a new node the value of curWriter.next will change
					if(curWriter.next == curWriter){
						firstElement = curWriter;
						//code to add when there is only one element in list
						newElement.previous = curWriter;
						newElement.next = curWriter;
						firstElement.next = newElement ;
						firstElement.previous = newElement;
						firstElement = newElement;
						System.out.println("adding second element into list and setting lock flag to false");
						flock2 = false;

					}
					//try to obtain the lock of current writer and next of current writer
					else{
						//code for adding when 2 elements have been added to the list
						//printing current writer for testing
						firstElement = curWriter;
						System.out.println("cur writer val is "+curWriter.value+"    "+curWriter.getClass());
						newElement.previous = firstElement;
						newElement.next = firstElement.next;
						firstElement.next.previous = newElement;
						firstElement.next = newElement;
						firstElement = newElement; 

					}
					System.out.println("object retained by temp node should be same as old curWriternext-->"+tempNode+"** "+Thread.currentThread().getName());
					if(flock1){
						curWriter.mylock.unlock();
						curWriter.ltaken.signalAll();
						flock1=false;
					}
					System.out.println(" flock2 y are you going in "+flock2);
					if(flock2){
						tempNode.mylock.unlock();
						//curWriter.next.next.ltaken.signalAll();
						flock2=false;
					}
				}
			}
			else{
				System.out.println(" ************Error the current element passed has no value ************** ");
				return false;
			}
			return true;
			//return super.insertAfter(val);
		}
		public  boolean delete(){
			//try to get lock of cursor before operation
			return super.delete();
		}
	}

}
