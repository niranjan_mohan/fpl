package old;

import java.util.concurrent.atomic.AtomicInteger;

public class CDLListRW {
	private AtomicInteger lock =new AtomicInteger();
	Object lockobj;
	boolean writerQ;
	int temp;
	
	 CDLListRW(){
		lock.set(1);
		lockobj = new Object();
	}

	public CDLListRW lockRead(){
		//if not writers are in Q
		if( lock.get() >1){
			lock.incrementAndGet();
		}
		else if(lock.get() == 1 ){
			while(!lock.compareAndSet(1, 2)){
				try {
					lockobj.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				synchronized(lockobj){
					
				}
			}
		}
		else{//when zero
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return this;
		
	}
	public void unlockRead(){
		if(lock.get() > 1){
			System.out.println("decreamenting reader");
			lock.decrementAndGet();
		}
		//for readers in buffer value will be negative 
		else if(lock.get()< -1){
			lock.incrementAndGet();
		}
		else{
			//should not be here
			System.out.println("in else in unlock writer");
		}
	}
	public CDLListRW lockWrite(){
		if((temp=lock.get()) > 1){
			lock.set((temp*-1));
		}
		else if(lock.get() <= 0)	{
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			while(!lock.compareAndSet(1, 0)){
				try {
					lockobj.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			synchronized(lockobj){
				
			}
		}
		return this;
	}
	public void unlockWrite(){
		lock.decrementAndGet();
	}

}
