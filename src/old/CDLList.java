package old;




import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Niranjan Mohan
 * nmohan4@buffalo.edu
 */
public class CDLList<T> {
	Element firstElement;
	Element headElement;
	Writer writer;
	
	
	public CDLList(T v){
		firstElement = new Element(v);
		firstElement.next = firstElement;
		firstElement.previous = firstElement;
		headElement = firstElement;
	}
	public CDLList() {
		// TODO Auto-generated constructor stub
	}
	public class Element{
		T value;
		Element next;
		Element previous;
		ReadWriteLock rwLock ;
		Element(T value){
			this.value = value;
			rwLock = new ReadWriteLock();
		}
		public T value(){
			return value;
		}
	}
	public Element head(){
		return headElement;
	}
	public Cursor reader(Element from){
		//find out the element and then pass the reader from that element 
		Cursor cursor = new Cursor();
		boolean flag = true;int count =0;
		while (flag){
			//search list for element
			if(cursor.current() == from ){
				//System.out.println("Element found");
				return cursor;
			}
			cursor.next();
			if(cursor.current() == headElement)
				count ++;
			if(count == 2){
				//System.out.println("Element not found in list returning the firstElement to reader");
				throw new RuntimeException("Invalid element");
			}
		}
		return cursor;

	}
	public class Cursor {
		Element curElement;
		
		Cursor(Element from){
			this.curElement = from;
		}
		Cursor(){
			this.curElement = firstElement;
		}
		public Element current(){
			return curElement;
		}
		public void previous(){
			curElement = curElement.previous;
		}
		public void next(){
			curElement = curElement.next;
		}
		public Writer writer(){
			//f(writer == null)
				writer = new Writer(curElement);
			return writer;
		}
	}
	public class Writer{
		Element curWriter;
		//boolean cursorLock;
		Writer(Element curElement){
			this.curWriter = curElement;
			//test lines need to remove
			//if(curWriter.value == null)
				//System.out.println("******************Error Curwriter is null******************************** ");
		}
		Writer(){
			if(curWriter !=null )
				this.curWriter = headElement;
		}

		public boolean insertBefore(T val){
			//try to get the lock of the writer
			//cursorLock = this.
			
			
			//check if list is empty
			Element newElement = new Element(val);
			if(curWriter != null){

				if(curWriter.next == curWriter){
					//code to add when there is only one element in list
					firstElement = curWriter;
					//System.out.println("adding item 2 into the list");
					newElement.previous = firstElement;
					newElement.next = firstElement;
					firstElement.next = newElement;
					firstElement.previous = newElement;
					firstElement = newElement;
					
				}
				else{
					//code for adding when 2 elements have been added to the list
					firstElement = curWriter;
//					System.out.println("cur writer val is "+curWriter.value);
//					System.out.println("adding 2 elements adding the others");
					newElement.next = firstElement;
					newElement.previous = firstElement.previous;
					firstElement.previous.next = newElement;
					firstElement.previous = newElement;
					firstElement = newElement; 
					
				}
				//System.out.println("value of head element in CDLIST is "+headElement.value);
			}
			else{
				//System.out.println(" ************Error the current element passed has no value ************** ");
				return false;
			}
			return true;
		}
		public boolean insertAfter(T val){
			//check if list is empty
			Element newElement = new Element(val);
			if(curWriter != null){

				if(curWriter.next == curWriter){
					firstElement = curWriter;
					//code to add when there is only one element in list
					newElement.previous = curWriter;
					newElement.next = curWriter;
					firstElement.next = newElement ;
					firstElement.previous = newElement;
					firstElement = newElement;
					
				}
				else{
					//code for adding when 2 elements have been added to the list
					//printing current writer for testing
					firstElement = curWriter;
					//System.out.println("cur writer val is "+curWriter.value);
					newElement.previous = firstElement;
					newElement.next = firstElement.next;
					firstElement.next.previous = newElement;
					firstElement.next = newElement;
					firstElement = newElement; 
				}
				//System.out.println("value of head element is "+head().value);
			}
			else{
				//System.out.println(" ************Error the current element passed has no value ************** ");
				return false;
			}
			return true;
		}
		public boolean delete(){
			//check if the element is the head element
			//System.out.println("value of writer"+curWriter.value);
			if(curWriter != headElement){
				curWriter.previous.next = curWriter.next;
				curWriter.next.previous = curWriter.previous;
				curWriter.next =null;
				curWriter.previous=null;
				curWriter = null;
				
			}
			else {
				//System.out.println("cannot delete head Element");
				return false;
			}
			return true;
		}
	}

	//Code written for testing should be deleted
//	public void displayAll(){
//		//System.out.println("moving forward");
//		Element curElement = headElement;
//		firstElement= headElement;
//		System.out.println("head element-->"+headElement);
//		outerloop:
//			while (firstElement != null){
//				
//				System.out.println("element ->"+firstElement.value);
//				if(firstElement.value == null){
//					//System.out.println("Dont get Shocked this is a test Code LOL!!!!");
//				}
//				firstElement = firstElement.next;
//				if (firstElement == curElement){
//					System.out.println("head element is "+headElement.value);
//					System.out.println("need to exit ");
//					break outerloop;
//				}
//			}
//
//	}


}
