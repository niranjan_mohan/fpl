package old;

/**
 * @author Niranjan Mohan
 * nmohan4@buffalo.edu
 */


public class ReadWriteLock {

	private volatile int readerBuff;
	//volatile int writerBuff;
	private  volatile int readerQ;
	private volatile int writerQ;
	private volatile boolean writerLock;//does "a" writer have the lock
	private volatile boolean readerLock;//does the reader have the lock
	


	public ReadWriteLock() {
		readerBuff =0;
		readerLock =false;
		writerLock = false;
	}
	public synchronized boolean lockRead(){
		//decrease reader count and return true
		readerQ++;
		while(! readerLock){
				if(writerLock){
					try {
						wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else if(! writerLock  ){
					readerLock = true;
				}
			
		}
//		System.out.println("After Reader Lock function Reader Count :"+ readerQ+" reader buffer :"+readerBuff+ "Writer Q:"+ writerQ);
		if(readerLock){	
			return true;
		}
		return false;

	}
	public synchronized boolean unlockRead(){
		//do I need to unlock Read
		if((readerLock && readerBuff == 0 && writerQ >0)){
			
//				System.out.println(" reader going to notify all");
				if(readerLock && readerBuff ==0){
					notifyAll();
					readerLock =false;
				}

			}
		
		if(readerBuff >0)
			readerBuff--;
		else{
			if(readerQ == 0)
				System.out.println("no");
//			System.out.println("decreasing reader Q:"+ readerQ+ "reader buffer:"+ readerBuff);
			readerQ--;
			if(readerQ== 0 && readerLock)
				readerLock=false;
		}
		
//		System.out.println("Reader unlock function reader q :"+readerQ+ "  reader buffer :"+ readerBuff+" Writer Q:"+ writerQ);
		return true;
	}
	public synchronized boolean lockWrite(){
		writerQ++;
		boolean waitFlag = false;
//		System.out.println("lock writer ->reader count:"+readerQ+" writer count:"+writerQ+"  reader lock:"+ readerLock+"  writer lock :"+ writerLock);
		if(readerLock){
			if(writerQ > 1){
				waitFlag= true;
			}
			else if(writerQ == 1 && readerBuff ==0 ){
				//inform readers that writer is in Q and load fairness buffer
				readerBuff = readerQ;
				readerQ =0;
				waitFlag = true;
			}
		}
		else if (writerLock){
			waitFlag=true;
		}
		else if(! writerLock && ! readerLock){
			//Got the writer LOCK !!
//			System.out.println("got Write Lock");
			writerLock = true;
			waitFlag = false;
		}
		else{
			System.out.println("should not enter this block");
		}
		while (waitFlag){
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(! writerLock && ! readerLock){
				//Got the writer LOCK !!
//				System.out.println("got Write Lock");
				writerLock = true;
				waitFlag = false;
			}
		}
		return true;
	}
	public synchronized boolean unlockWrite(){

		//change flag from reader to writer lock
		writerQ--;
		writerLock =false;
//		System.out.println("writer count after unlocking lock :"+writerQ+ "reader Q:"+readerQ+"readerbuffer:"+readerBuff);
		if(readerQ > 0 || writerQ > 0){
//			System.out.println("writer is going to notify");
			notifyAll();
		}
		return true;
	}
}
//public boolean canGetWLock() {}
